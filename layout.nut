///////////////////////////////////////////////////
//
// Attract-Mode Frontend - Orbit layout
//
///////////////////////////////////////////////////
//
// NOTES:
//
// - The looping static video is from the rec room website at:
//   http://recroomhq.com/downloads/2010/04/14/tv-static-freebie.html
//   It is licensed under the Creative Commons Attribution 3.0 License
//
///////////////////////////////////////////////////
class UserConfig {
	</ label="Orbit Artwork", help="The artwork to spin into orbit", options="marquee,flyer,wheel", order=1 />
	orbit_art="marquee";

	</ label="Sprinkle Artwork", help="Artwork showing 1P/2P and other info", options="sprinkle", order=2 />
	sprinkle_art="sprinkle";

	</ label="Bloom Effect", help="Enable Bloom Effect (requires shader support)", options="Yes,No", order=3 />
	enable_bloom="Yes";

	</ label="Background", help="The filename of the image or video to display in the background", order=4 />
	bg_image="background.png";

	</ label="Foreground", help="The filename of the image or video to display in the foreground", order=5 />
	fg_image="foreground.png";

	</ label="Satellite Count", help="The number of orbiting artworks", options="5,7,9,11,13,15", order=6 />
	count="5";

	</ label="Spin Time", help="The amount of time it takes to spin to the next selection (in milliseconds)", order=7 />
	spin_ms="120";

	</ label="Static Effect", help="Enable static effect", options="Yes,No", order=8 />
	static_effect="Yes";
}

fe.load_module( "conveyor" );
local my_config = fe.get_config();

fe.layout.width = 1920
fe.layout.height = 1080

const MWIDTH = 640;
const MHEIGHT = 192;
const SNAPBG_ALPHA = 200;

local num_sats = fe.layout.page_size = my_config["count"].tointeger();
local progress_correction = 1.0 / ( num_sats * 2 );

local spin_ms = 120;
try {
	spin_ms = my_config["spin_ms"].tointeger();
} catch ( e ) {}

function get_y( x )
{
	// 270^2 = 72900
	return 470;
//	return ( 200 + sqrt( 72900 - pow( x - 400, 2 ) ));
}

function set_bright( x, o )
{
	o.set_rgb( x, x, x );
}

//
// Create a class to contain an orbit artwork
//
class Satallite extends ConveyorSlot
{
	static x_lookup = [ -240.0, 60.0, 960.0, 1860.0, 2160.0 ];
	static s_lookup = [ 0.25, 0.5, 1.0, 0.5, 0.25 ];
	static x_slices = 4;
	static y_fixed = 921;

	constructor()
	{
		local o = fe.add_artwork( my_config["orbit_art"] );
		o.preserve_aspect_ratio=true;
		o.video_flags = Vid.ImagesOnly;

		base.constructor( o );
	}

	//
	// Place, scale and set the colour of the artwork based
	// on the value of "progress" which ranges from 0.0-1.0
	//
	function on_progress( progress, var )
	{
		local scale;
		local new_x;
		progress += progress_correction;

		if ( progress >= 1.0 )
		{
			scale = s_lookup[ x_slices ];
			new_x = x_lookup[ x_slices ];
		}
		else if ( progress < 0 )
		{
			scale = s_lookup[ 0 ];
			new_x = x_lookup[ 0 ];
		}
		else
		{
			local factor = progress * x_slices;
			local slice = ( factor ).tointeger();
			factor = factor - slice;

			scale = s_lookup[ slice ]
				+ (s_lookup[slice+1] - s_lookup[slice]) * factor;

			new_x = x_lookup[ slice ]
				+ (x_lookup[slice+1] - x_lookup[slice]) * factor;
		}

		m_obj.width = MWIDTH * scale;
		m_obj.height = MHEIGHT * scale;
		m_obj.x = new_x - m_obj.width / 2;
		m_obj.y = y_fixed - m_obj.height / 2;

		local bright = scale * 255;
		if (bright > 255)
		{
			bright = 255;
		}

		set_bright( bright, m_obj );
	}
}

//
// Initialize background image if configured
//
if ( my_config[ "bg_image" ].len() > 0 )
	fe.add_image( my_config[ "bg_image" ],
			0, 0, fe.layout.width, fe.layout.height );

//
// Initialize the video frame
//
local snapbg=null;
if ( my_config[ "static_effect" ] == "Yes" )
{
	snapbg = fe.add_image(
		"static.mp4",
		448, 96, 1024, 576 );

	snapbg.set_rgb( 150, 150, 150 );
	snapbg.alpha = SNAPBG_ALPHA;
}
else
{
	local temp = fe.add_text(
		"",
		448, 96, 1024, 576 );
	temp.bg_alpha = SNAPBG_ALPHA;
}

local snap = fe.add_artwork( "snap", 448, 96, 1024, 576 );
snap.trigger = Transition.EndNavigation;

local sprinkle = fe.add_artwork ( "sprinkle", 1534, 187, 320, 384)
sprinkle.trigger = Transition.EndNavigation;

//
// Initialize misc text
//
// local l = fe.add_text( "[FilterName] [[ListEntry]/[ListSize]]", 400, 580, 400, 20 );
// l.set_rgb( 180, 180, 70 );
// l.align = Align.Right;

// local l = fe.add_text( "[Category]", 0, 580, 400, 20 );
// l.set_rgb( 180, 180, 70 );
// l.align = Align.Left;

//
// Initialize the orbit artworks with selection at the top
// of the draw order
//
local sats = [];
for ( local i=0; i < num_sats  / 2; i++ )
	sats.append( Satallite() );

for ( local i=0; i < ( num_sats + 1 ) / 2; i++ )
	sats.insert( num_sats / 2, Satallite() );

//
// Initialize a conveyor to control the artworks
//
local orbital = Conveyor();
orbital.transition_ms = spin_ms;
orbital.transition_swap_point = 1.0;
orbital.set_slots( sats );

//
// Title text
//
//local l = fe.add_text( "[DisplayName]", 0, 0, 800, 55 );
//l.set_rgb( 180, 180, 70 ); 
//l.style = Style.Bold;

//
// Set the shader effect if configured
//
if ( my_config["enable_bloom"] == "Yes" )
{
	local sh = fe.add_shader( Shader.Fragment, "bloom_shader.frag" );
	sh.set_texture_param("bgl_RenderedTexture"); 

	sats[ sats.len() / 2 ].m_obj.shader = sh;
//	l.shader = sh;
}

//
// Name text w/ black outline
//
local NAME_Y = 728;
local NAME_SIZE = 50;
fe.add_text( "[Title], [Manufacturer] [Year]", -1, NAME_Y, fe.layout.width, NAME_SIZE )
	.set_rgb( 0, 0, 0 );

fe.add_text( "[Title], [Manufacturer] [Year]", 1, NAME_Y, fe.layout.width, NAME_SIZE )
	.set_rgb( 0, 0, 0 );

fe.add_text( "[Title], [Manufacturer] [Year]", 0, NAME_Y - 1, fe.layout.width, NAME_SIZE )
	.set_rgb( 0, 0, 0 );

fe.add_text( "[Title], [Manufacturer] [Year]", 0, NAME_Y + 1, fe.layout.width, NAME_SIZE )
	.set_rgb( 0, 0, 0 );

fe.add_text( "[Title], [Manufacturer] [Year]", 0, NAME_Y, fe.layout.width, NAME_SIZE )
	.set_rgb( 255, 255, 255 );

//
// Initialize foreground image if configured
//
if ( my_config[ "fg_image" ].len() > 0 )
	fe.add_image( my_config[ "fg_image" ],
			0, 0, fe.layout.width, fe.layout.height );

//
// Add fade effect when moving to/from the layout or a game
//
fe.add_transition_callback( "orbit_transition" );
function orbit_transition( ttype, var, ttime )
{
	switch ( ttype )
	{
	case Transition.ToNewSelection:
		if ( snapbg )
		{
			if ( snap.file_name.len() > 0 )
			{
				if ( ttime < spin_ms )
				{
					snap.alpha = 255 - 255.0 * ttime / spin_ms;
					return true;
				}
			}
			snap.file_name="";
			snap.alpha=0;
		}
		break;

	case Transition.EndNavigation:
		if ( snapbg )
		{
			if ( ttime < spin_ms )
			{
				snap.alpha = 255.0 * ttime / spin_ms;
				return true;
			}
			snap.alpha = 255;
		}
		break;

	case Transition.StartLayout:
	case Transition.FromGame:
		if ( ttime < 255 )
		{
			foreach (o in fe.obj)
				o.alpha = ttime;

			return true;
		}
		else
		{
			foreach (o in fe.obj)
				o.alpha = 255;
		}
		if ( snapbg )
			snapbg.alpha=SNAPBG_ALPHA;
		break;

	case Transition.EndLayout:
	case Transition.ToGame:
		if ( ttime < 255 )
		{
			foreach (o in fe.obj)
				o.alpha = 255 - ttime;

			return true;
		}
		else
		{
			local old_alpha;
			foreach (o in fe.obj)
			{
				old_alpha = o.alpha;
				o.alpha = 0;
			}

			if ( old_alpha != 0 )
				return true;
		}

		break;
	}

	return false;
}
