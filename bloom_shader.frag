uniform sampler2D bgl_RenderedTexture;
	
void main()
{
	vec4 sum = vec4(0);
	int i;
	int j;

	for (i = -4; i <= 4; ++i)
		for (j = -4; j <= 4; ++j)
			sum += texture2D(bgl_RenderedTexture, gl_TexCoord[0]+vec2(i,j)*0.004) * (8 - abs(i) - abs(j)) / 250;

	vec4 pix = texture2D(bgl_RenderedTexture, gl_TexCoord[0]);
	if (max(pix.r, max(pix.g, pix.b)) * pix.a < 0.3) {
		gl_FragColor = pix + sum;
	} else {
		gl_FragColor = pix + sum * sum;
	}
//	gl_FragColor = texture2D(bgl_RenderedTexture, gl_TexCoord[0]) + sum * sum;
//	gl_FragColor = sum * sum;
}
